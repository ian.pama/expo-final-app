import React from 'react';
import { StackNavigator } from 'react-navigation';
import HomeScreen from './screens/HomeScreen'
import TextScreen from './screens/TextScreen'
import TextScreen2 from './screens/TextScreen2'
import TextScreen3 from './screens/TextScreen3'
import TextScreen4 from './screens/TextScreen4'
import TextScreen5 from './screens/TextScreen5'

const RootStack = StackNavigator (
  {
    Home: {
      screen: HomeScreen,
    },
   
    TextScreen: {
      screen: TextScreen,
    },
    
    TextScreen2: {
      screen: TextScreen2,
    },
    
    TextScreen3: {
      screen: TextScreen3,
    },
    
    TextScreen4: {
      screen: TextScreen4,
    },

    TextScreen5: {
      screen: TextScreen5,
    },
  
  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}

