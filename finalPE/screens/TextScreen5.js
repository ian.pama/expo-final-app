//Different Types of Rice and Their Uses
import React, { Component } from 'react';
import { Text, StyleSheet, Button, View } from 'react-native';

export default class TextScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      titleText: "Types and Their Uses",
      bodyText: 'ARBORIO RICE - a medium grain rice that is wider in size and has a characteristic white dot at the center of the grain. It is named after the town of Arborio in the Po Valley of Italy, where it is grown. Due to the high starch content of Arborio rice, it has a slightly chewy and sticky consistency and develops a creamy texture when cooked',
      bodyText1: 'BASMATI RICE - grains are long, dry, and separate. They impart a pleasant, nutty aroma and flavor in any dish. It is common in Indian and Asian cuisine, but it can be used in a variety of flavorful recipes. Serve it plain or with fresh herbs, green onions, coconut, or vegetables for a signature pilaf.',
      bodyText2: 'BROWN RICE - grains have a chewy texture when cooked. They impart a pleasant, slightly nutty flavor in any dish. The nutritious bran layers are left on brown rice so it can retain its natural goodness and tan color. Rich in vitamins and minerals, brown rice is a 100% whole grain food. It is a versatile rice that becomes light and fluffy when cooked, ensuring it wont stick together.',
      bodyText3: 'JASMINE RICE - will bring an exotic flair and flavorful accent to any dish. It develops a pleasant jasmine aroma while it is cooking. Use it when making a variety of traditional Asian dishes, including curries and stir-frys. The moist, soft texture is ideal for soaking up spices and flavors.',
      bodyText4: 'WHITE RICE - has a slightly sticky consistency thats useful in stuffing, casseroles, and stir-fry dishes. It is arguably the most familiar and easily recognizable rice in traditional American recipes, and its also popular in Asian and Mexican cuisine. Compared to other varieties of rice, it has a mild flavor and light and fluffy texture when cooked.',

    };
  }

  render() {
    return (
      <View >
        <Text style={styles.titleText} onPress={this.onPressTitle}>
          {this.state.titleText}{'\n'}{'\n'}
        </Text>
        <Text numberOfLines={10}>
          {this.state.bodyText}{'\n'}{'\n'}
        </Text>
        <Text numberOfLines={10}>
          {this.state.bodyText1}{'\n'}{'\n'}
        </Text>
        <Text numberOfLines={10}>
          {this.state.bodyText2}{'\n'}{'\n'}
        </Text>
        <Text numberOfLines={10}>
          {this.state.bodyText3}{'\n'}{'\n'}
        </Text>
        <Text numberOfLines={10}>
          {this.state.bodyText4}{'\n'}{'\n'}{'\n'}{'\n'}{'\n'}{'\n'}
        </Text>
        <Button style={styles.space}
            title="Home"
            onPress={() => this.props.navigation.navigate('Home')}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  baseText: {
    fontFamily: 'Tahoma',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
