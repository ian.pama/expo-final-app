import React, { Component } from 'react';
import { Text, StyleSheet, Button, View } from 'react-native';

export default class TextScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      titleText: "Texture",
      bodyText: 'STICKY RICE - also known as sweet rice, sticky rice is grown mainly in Southeast and East Asia and is used in many traditional Asian dishes, desserts, and sweets. When cooked, sticky rice is especially sticky and is often ground into rice flour.',
      bodyText1: 'PARBOILED RICE - this “rough” rice has gone through a steam-pressure process before milling that gelatinizes the starch in the grain. This process produces a more separate grain that is light and fluffy when cooked. Converted rice is a type of parboiled rice that has been further pre-cooked, which ultimately allows you to whip up dishes of rice even faster.',
  };
}

  render() {
    return (
      <View >
        <Text style={styles.titleText} onPress={this.onPressTitle}>
          {this.state.titleText}{'\n'}{'\n'}
        </Text>
        <Text numberOfLines={10}>
        
          {this.state.bodyText}{'\n'}{'\n'}
          </Text>
        <Text numberOfLines={10}>
          {this.state.bodyText}{'\n'}{'\n'}{'\n'}{'\n'}{'\n'}{'\n'}
        </Text>
        <Button style={styles.space}
            title="Home"
            onPress={() => this.props.navigation.navigate('Home')}
        />
      </View>
    );
  }
}

 const styles = StyleSheet.create({
  baseText: {
    fontFamily: 'Tahoma',
  },
  titleText: {
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
