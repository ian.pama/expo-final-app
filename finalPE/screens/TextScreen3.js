import React, { Component } from 'react';
import { Text, StyleSheet, Button, View } from 'react-native';

export default class TextScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      titleText: "Color",
      bodyText: 'POLISHED RICE - the term “polished” simply refers to white rice that has had its outer brown layer of bran and germ removed. Rice that has shed its bran layers can also be referred to as "milled rice."',
      bodyText1: 'BROWN RICE - this healthful rice sheds its outer husk and retains its bran and germ layers that give it a characteristic tan color. Though brown rice takes a little longer to cook than white rice, the nutrient-dense layers are rich in vitamins and minerals.',
      bodyText2: 'FORBIDDEN RICE - high in nutritional value, this rice is also known as black rice and has a mild nutty flavor. Slightly sticky when cooked, it is used in a variety of Chinese or Thai dishes, including Chinese black rice cake and mango sticky rice. Mix it with white rice, and it also adds color to any rice pilaf or rice bowl.',
      bodyText3: 'WILD RICE - wild rice grains are harvested from the genus Zizania of grasses. High in protein, wild rice adds a colorful, exotic flair to any rice dish. Serve it with stir frys, mushroom soups, or casseroles for something new.',

    };
      
  }

  render() {
    return (
      <View >
        <Text style={styles.titleText} onPress={this.onPressTitle}>
          {this.state.titleText}{'\n'}{'\n'}
        </Text>
        <Text numberOfLines={10}>
          {this.state.bodyText}{'\n'}{'\n'}
        </Text>
        <Text numberOfLines={10}>
          {this.state.bodyText1}{'\n'}{'\n'}
        </Text>
        <Text numberOfLines={10}>
          {this.state.bodyText2}{'\n'}{'\n'}
        </Text>
        <Text numberOfLines={10}>
          {this.state.bodyText3}{'\n'}{'\n'}{'\n'}{'\n'}{'\n'}{'\n'}
        </Text>
        <Button style={styles.space}
            title="Home"
            onPress={() => this.props.navigation.navigate('Home')}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  baseText: {
    fontFamily: 'Tahoma',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
