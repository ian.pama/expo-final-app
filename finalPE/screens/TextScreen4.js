//Aroma
import React, { Component } from 'react';
import { Text, StyleSheet, Button, View } from 'react-native';

export default class TextScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      titleText: "Aroma",
      bodyText: 'AROMA is another factor to consider when cooking with rice. Certain rice varieties give off pleasing fragrances while being cooked. Add a sensory element to your guests dining experience with these rice types.',
      bodyText1: 'BASMATI RICE - Basmati rice is a type of long-grain rice that is popular among Indian cuisine and other ethnic dishes. Cooked basmati rice imparts a subtle nutty or popcorn-like flavor and aroma.',
      bodyText2: 'JASMINE RICE - Jasmine rice, sometimes known as Thai fragrant rice, is a type of long grain rice with a long kernel and slightly sticky texture when cooked. Use it to infuse a subtle jasmine flavor and aroma into your dishes.',

     };
  }

  render() {
    return (
      <View >
        <Text style={styles.titleText} onPress={this.onPressTitle}>
          {this.state.titleText}{'\n'}{'\n'}
        </Text>
        <Text numberOfLines={10}>
          {this.state.bodyText}{'\n'}{'\n'}
        </Text>
        <Text numberOfLines={10}>
          {this.state.bodyText1}{'\n'}{'\n'}
        </Text>
        <Text numberOfLines={10}>
          {this.state.bodyText2}{'\n'}{'\n'}{'\n'}{'\n'}{'\n'}{'\n'}
        </Text>

        <Button style={styles.space}
            title="Home"
            onPress={() => this.props.navigation.navigate('Home')}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  baseText: {
    fontFamily: 'Tahoma',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
