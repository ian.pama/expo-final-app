import React, { Component } from 'react';
import { Text, StyleSheet, Button, View } from 'react-native';

export default class TextScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      titleText: "Length and Shape",
      bodyText: 'LONG GRAIN RICE - this rice has milled grains that are at least three to four times as long as they are wide. Due to its starch composition, it is separate, light and fluffy when cooked.',
      bodyText1: 'MEDUIM GRAIN RICE - when compared to long grain rice, medium grain rice has a shorter, wider kernel. Since the cooked grains are more moist and tender than long grain rice, the rice has a greater tendency to stick together.',
      bodyText2: 'SHORT GRAIN RICE - featuring grains that are less than twice as long as they are wide, this rice is short and best for sushi. It has a sticky texture when cooked.',
    };
  }

  render() {
    return (
      <View >
        <Text style={styles.titleText} onPress={this.onPressTitle}>
          {this.state.titleText}{'\n'}{'\n'}
        </Text>
        <Text numberOfLines={10}>
          {this.state.bodyText}{'\n'}{'\n'}
        </Text> 
        <Text numberOfLines={10}>
          {this.state.bodyText1}{'\n'}{'\n'}
        </Text>
        <Text numberOfLines={10}>  
          {this.state.bodyText2}{'\n'}{'\n'}{'\n'}{'\n'}{'\n'}{'\n'}
        </Text>
        <Button style={styles.space}
            title="Home"
            onPress={() => this.props.navigation.navigate('Home')}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  baseText: {
    fontFamily: 'Tahoma',
  },
  titleText: {
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
