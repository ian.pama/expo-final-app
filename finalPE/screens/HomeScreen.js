import React, {Component} from 'react';
import { StyleSheet, Text, View, Button, ScrollView } from 'react-native';

class HomeScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
              <ScrollView>
              <Text style={styles.title}>RICE TRIVIA{'\n'}{'\n'}</Text>
            
              <Button style={styles.space}
                  title="Length and Shape"
                  onPress={() => this.props.navigation.navigate('TextScreen')}
              />
               <Button style={styles.space}
                  title="Texture"
                  onPress={() => this.props.navigation.navigate('TextScreen2')}
              />
               <Button style={styles.space}
                  title="Color"
                  onPress={() => this.props.navigation.navigate('TextScreen3')}
              />
               <Button style={styles.space}
                  title="Aroma"
                  onPress={() => this.props.navigation.navigate('TextScreen4')}
              />
               <Button style={styles.space} 
                  title="Types and Their Uses"
                  onPress={() => this.props.navigation.navigate('TextScreen5')}
              />
  
           
              </ScrollView>
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      //flex: 1,
     // backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      fontSize: 40,
      margin: 20,
      textAlign: 'center',
    },
    space: {
      margin: 50,
    }
  });

export default HomeScreen;
